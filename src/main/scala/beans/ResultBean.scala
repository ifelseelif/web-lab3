package beans

import java.util

import db.ResultBeanDAO
import javax.faces.context.FacesContext

import scala.jdk.CollectionConverters._

// Though this is Scala, it is still Java code :(
class ResultBean extends Serializable {
  private val serialVersionUID: Long = 1L
  private val resultBeanDAO = new ResultBeanDAO()
  private val sessionId: String = FacesContext.getCurrentInstance.getExternalContext.getSession(true).hashCode.toString

  var x: Array[Double] = Array(1.0)
  var r: Array[Double] = Array(1.0)
  var y: String = "0"

  //Эта штука нужна данных от графа
  var graphData = ""

  def getGraphData: String = graphData

  def setGraphData(data: String): Unit = graphData = data

  def getX: Array[Double] = x

  def setX(data: Array[Double]): Unit = this.x = data

  def getR: Array[Double] = r

  def setR(data: Array[Double]): Unit = this.r = data

  def getY: String = y

  def setY(y: String): Unit = y.toDoubleOption.foreach(_ => this.y = y)

  def addPoint(): Unit = {
    for {
      curX <- x.toList
      curY = y.toDouble
      curR <- r.toList
    } yield resultBeanDAO addPointToDB Point(curX, curY, curR, sessionId)
    ()
  }

  def getPoints: util.List[Point] =
    resultBeanDAO.showOwnersPoints(sessionId).getOrElse(List(Point(6, 6, 6, sessionId))).asJava

  def addPointViaGraph(): Unit = {
    val data: util.Map[String, String] = FacesContext.getCurrentInstance.getExternalContext.getRequestParameterMap
    setGraphData(data.getOrDefault("graph", "error"))
    if (!getGraphData.equals("error")) {
      val point = Point.fromGraphData(getGraphData, sessionId)
      resultBeanDAO.addPointToDB(point)
    }
  }
}