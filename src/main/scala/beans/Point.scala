package beans
import java.io.Serializable

import javax.persistence._
import org.eclipse.persistence.annotations.PrimaryKey

//Это bean нужен для таблицы результатов. Она хранит в себе x,y,r и результат проверки
@Entity
@Table(name = "POINT")
class Point {

  @Id
  @PrimaryKey
  @GeneratedValue(strategy = GenerationType.AUTO)
  protected var id: Int = 0

  @Column(name = "X")
  private var x: Double = 0

  @Column(name = "Y")
  private var y: Double = 0

  @Column(name = "R")
  private var r: Double = 0

  @Column(name="HIT")
  private var hit = false // Факт попадания в точку

  @Column(name = "OWNER")
  private var owner = ""

  def setOwner(sessionId: String): Unit = this.owner = sessionId

  def getOwner: String = owner

  def getX: Double = x

  def setX(x: Double): Unit = this.x = (x * 100).round.toDouble / 100

  def getY: Double = y

  def setY(y: Double): Unit = this.y = y.*(100).round.toDouble./(100)

  def getR: Double = r

  def setR(r: Double): Unit = this.r = (r * 100).round.toDouble / 100

  def defineHit(): Unit =
    setHit(
      List.apply[Point => Boolean](
        { case Point(x, y, r, _) => (x >= 0 && y >= 0) && (x * x + y * y <= r * r) && (r > 0) },
        { case Point(x, y, r, _) => (y >= 0 && x <= 0) && (x >= -r && y <= r / 2) },
        { case Point(x, y, r, _) => (y >= -x - r) && (y <= 0 && x <= 0) }
      ).exists(_(this))
    )

  def getHit: Boolean = hit

  def setHit(hit: Boolean): Unit = this.hit = hit

}

object Point extends Serializable {
  def unapply(point: Point): Option[(Double, Double, Double, String)] =
    Some((point.getX, point.getY, point.getR, point.getOwner))

  def apply(x: Double, y: Double, r: Double, sessionId : String): Point = {
    val point = new Point()
    point.setX(x)
    point.setY(y)
    point.setR(r)
    point.defineHit()
    point.setOwner(sessionId)
    point
  }

  def fromGraphData(encodedPoint: String, sessionId: String): Point = {
    if (encodedPoint == null || encodedPoint.isEmpty) Point(6, 6, 6, sessionId) else {
      val x :: y :: r :: Nil = encodedPoint.split(':').toList
      apply(x.toDouble, y.toDouble, r.toDouble, sessionId)
    }
  }
}
