package db

import beans.Point
import javax.persistence.{Persistence, TypedQuery}

import scala.jdk.CollectionConverters._
import scala.util.Try

class ResultBeanDAO(persistentUnitName: String = "db_oracle") {

  private lazy val s = Persistence.createEntityManagerFactory(persistentUnitName)
  private lazy val entityManager = s.createEntityManager

  def addPointToDB(point: Point): Try[Unit] = Try {
    val transaction = entityManager.getTransaction
    transaction.begin()
    entityManager.persist(point)
    transaction.commit()
  }

  def showOwnersPoints(sessionId: String): Try[List[Point]] = Try {
      val query: TypedQuery[Point] = entityManager.createQuery("SELECT p FROM Point p WHERE p.owner = :sessionId", classOf[Point])
      query.setParameter("sessionId", sessionId).getResultList.asScala.toList
  }

  override def finalize(): Unit = {
    entityManager.close()
  }

}
