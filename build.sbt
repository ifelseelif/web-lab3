name := "web-lab3"

version := "0.1"

scalaVersion := "2.13.1"


lazy val mainDependencies: Seq[ModuleID] = Seq(
  "com.oracle.ojdbc" % "ojdbc8" % "19.3.0.0",
  "javax.faces" % "javax.faces-api" % "2.3" % "provided",
  "org.eclipse.persistence" % "eclipselink" % "2.7.5",
  "org.primefaces" % "primefaces" % "7.0"
)

libraryDependencies ++= mainDependencies