const graphName = "canvas";
var globalRadius = NaN;

function drawFigure(radius, document) {

    if (radius === -1) {
        let last_val = NaN;
        let checks = document.querySelectorAll("input[type=checkbox]");
        for (let i = 0; i < checks.length; i++) {
            let check = checks[i];
            if (check.name.endsWith("param-r") && check.checked) {
                last_val = Number(check.value)
            }
        }
        radius = last_val;
        globalRadius = radius;
    }

    const canvas = document.getElementById(graphName);

    const context = canvas.getContext("2d");

    context.fillStyle = "#FFFFFF";
    context.fillRect(0, 0, 300, 300);

    context.fillStyle = "#03A9F4"; // цвет для заливки фигур
    context.strokeStyle = "#03A9F4";


// рисуем фигуры

    context.beginPath();
    context.arc(150, 150, 120, Math.PI + Math.PI / 2, 0); // круг
    context.lineTo(150, 150);
    context.fill();

    context.fillRect(30, 150, 120, -60); // прямоугольник

    context.moveTo(150, 150);
    context.lineTo(150, 270);
    context.lineTo(30, 150);
    context.fill();
    context.closePath();
    context.stroke();

    context.strokeStyle = "#000000";
    context.fillStyle = "#000000";
// рисуем ось x
    context.beginPath();
    context.moveTo(0, 150);
    context.lineTo(300, 150);
    context.lineTo(295, 145);
    context.moveTo(300, 150);
    context.lineTo(295, 155);

// рисуем ось y
    context.moveTo(150, 300);
    context.lineTo(150, 0);
    context.lineTo(145, 5);
    context.moveTo(150, 0);
    context.lineTo(155, 5);
    context.closePath();
    context.stroke();

    if (isNaN(radius)) {
        context.fillText('R', 160, 35);
        context.fillText('R/2', 160, 95);
        context.fillText('-R/2', 160, 214);
        context.fillText('-R', 160, 274);
        context.fillText('-R', 20, 140);
        context.fillText('-R/2', 70, 140);
        context.fillText('R/2', 197, 140);
        context.fillText('R', 265, 140);
    } else {
        context.fillText(radius, 160, 35);
        context.fillText(radius / 2, 160, 95);
        context.fillText(-radius / 2, 160, 214);
        context.fillText(-radius, 160, 274);
        context.fillText(-radius, 20, 140);
        context.fillText(-radius / 2, 70, 140);
        context.fillText(radius / 2, 197, 140);
        context.fillText(radius, 265, 140);
    }
    setTimeout(loadDots(),1000);
}

drawFigure(NaN, document);

function loadDotsByAjax(data) {
    if(data.status === "success"){
        loadDots()
    }
}

function loadDots() {
    let canvas = document.getElementById(graphName);
    let context = canvas.getContext("2d");
    if(isNaN(globalRadius)){return}
    let table = document.getElementsByTagName("tbody").item(2).rows;
    for (let i = 0; i < table.length; i++) {
        let cells = table.item(i).cells;
        let dotCoords = {
            x: parseFloat(cells.item(0).innerHTML.trim()) / globalRadius * 125 + 150,
            y: 150 - parseFloat(cells.item(1).innerHTML.trim()) / globalRadius * 125,
            color: ""
        };
        if(cells.item(3).innerText.trim() === "Нет") {dotCoords.color =  "#ff2634"} else { dotCoords.color = "#11ff38"}
        console.log(dotCoords.color);
        context.beginPath();
        context.fillStyle = dotCoords.color;
        context.arc(dotCoords.x, dotCoords.y, 4, 0, 2 * Math.PI);
        context.fill();
    }
}

