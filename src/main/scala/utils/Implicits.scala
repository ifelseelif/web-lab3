package utils

import scala.concurrent.{ExecutionContext, Future}

object Implicits {

  implicit class RichFuture[T](future: => Future[T]){
    def retryNTimes(n: Int)(implicit ec: ExecutionContext): Future[T] = {
      assert(n > 0)
      future recoverWith {
        case exp: Throwable =>
          println(s"Handled an exception ${exp.getMessage} | Will retry $n more times")
          retryNTimes(n - 1) recoverWith (_ => Future.failed(exp))
      }
    }
  }

}
